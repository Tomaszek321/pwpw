package pl.pwpw.playground.domian;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import pl.pwpw.playground.application.ExternalFile;
import pl.pwpw.playground.domian.model.Attachment;

@Mapper
public interface AttachmentConverter {

    @Mapping(target = "attachmentType", source = "type")
    Attachment convertToAttachments(final ExternalFile file);
}
