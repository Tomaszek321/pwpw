package pl.pwpw.playground.domian.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.pwpw.playground.domian.model.Application;
import pl.pwpw.playground.domian.model.projection.ApplicationDetailsProjection;
import pl.pwpw.playground.domian.model.projection.ContactDetailsProjection;

import java.util.List;

@Repository
public interface ApplicationRepository extends JpaRepository<Application, Long> {

   @Query(value = "SELECT EMAIL_ADDRESS, PHONE_NUMBER FROM APPLICATION a WHERE APPLICATION_NUMBER = :appNr",
           nativeQuery = true)
    ContactDetailsProjection findByApplicationNr(@Param("appNr") String appNr);

   @Query(value = "SELECT APPLICATION_TYPE, APPLICATION_NUMBER, FIRST_NAME FROM APPLICATION A WHERE EMAIL_ADDRESS = :email",
   nativeQuery = true)
    List<ApplicationDetailsProjection> findByEmail(@Param("email") String email);
}
