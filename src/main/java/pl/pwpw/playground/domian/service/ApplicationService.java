package pl.pwpw.playground.domian.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.pwpw.playground.domian.model.Application;
import pl.pwpw.playground.domian.model.projection.ApplicationDetailsProjection;
import pl.pwpw.playground.domian.model.projection.ContactDetailsProjection;
import pl.pwpw.playground.domian.repository.ApplicationRepository;

import java.util.List;

@Service
@AllArgsConstructor
public class ApplicationService {

    private final ApplicationRepository repository;

    public Application findById(final Long id) {
        return repository.getOne(id);
    }

    public ContactDetailsProjection findContactDetailsByApplicationNr(final String appNr) {
        return repository.findByApplicationNr(appNr);
    }

    public List<ApplicationDetailsProjection> findApplicationDetailsByEmail(final String email) {
        return repository.findByEmail(email);
    }
}
