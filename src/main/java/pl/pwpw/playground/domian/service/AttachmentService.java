package pl.pwpw.playground.domian.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.pwpw.playground.application.ExternalFile;
import pl.pwpw.playground.domian.AttachmentConverter;
import pl.pwpw.playground.domian.model.Attachment;
import pl.pwpw.playground.domian.repository.AttachmentRepository;

@Service
@AllArgsConstructor
public class AttachmentService {

    private final AttachmentConverter converter;

    private final AttachmentRepository repository;

    public Attachment save(final ExternalFile file) {
        final Attachment attachment = converter.convertToAttachments(file);

        return repository.save(attachment);
    }

}
