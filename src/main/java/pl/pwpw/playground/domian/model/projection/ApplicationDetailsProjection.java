package pl.pwpw.playground.domian.model.projection;

import org.springframework.beans.factory.annotation.Value;

public interface ApplicationDetailsProjection {

    @Value("#{target.application_type}")
    String getType();

    @Value("#{target.application_number}")
    String getAppNr();

    String getLastName();

}
