package pl.pwpw.playground.domian.model;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.Setter;


import javax.persistence.*;
import java.time.LocalDateTime;

@Table(name = "attachment")
@NoArgsConstructor
@Entity
@Setter
@EqualsAndHashCode
public class Attachment {

    @Id
    @SequenceGenerator(name = "attachment_seq_gen")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "attachment_seq_gen")
    private Long id;

    @Column(name = "create_date")
    private LocalDateTime createDate;

    @Lob
    @Column(name = "content")
    private byte[] content;

    @Column(name = "attachment_type")
    private String attachmentType;

    @ManyToOne(fetch = FetchType.LAZY)
    private Application application;

    @PrePersist
    protected void onCreate() {
        createDate = LocalDateTime.now();
    }



}
