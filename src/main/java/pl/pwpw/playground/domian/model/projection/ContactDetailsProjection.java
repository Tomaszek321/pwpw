package pl.pwpw.playground.domian.model.projection;

import org.springframework.beans.factory.annotation.Value;

public interface ContactDetailsProjection {

    @Value("#{target.email_address}")
    String getEmail();

    @Value("#{target.phone_number}")
    String getPhone();
}
