package pl.pwpw.playground.domian.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

/**
 *
 */
@Data
@NoArgsConstructor
@Entity
public class Application {
    @Id
    @SequenceGenerator(name = "app_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "app_id_seq")
    private Long appId;
    @Embedded
    private ApplicationNumber applicationNumber;
    private String firstName;
    private String lastName;
    @Embedded
    private ContactDetails contactDetails;
    @Enumerated(EnumType.STRING)
    private ApplicationType applicationType;

    @OneToMany(mappedBy = "application", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Attachment> attachments;

    public void addAttachment(final Attachment attachment) {
        attachments.add(attachment);
        attachment.setApplication(this);
    }

    public void removeAttachment(final Attachment attachment) {
        attachments.remove(attachment);
        attachment.setApplication(null);
    }




}
