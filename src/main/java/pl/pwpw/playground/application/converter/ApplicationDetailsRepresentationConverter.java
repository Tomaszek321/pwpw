package pl.pwpw.playground.application.converter;

import org.mapstruct.Mapper;
import pl.pwpw.playground.application.ApplicationDetailsRepresentation;
import pl.pwpw.playground.domian.model.projection.ApplicationDetailsProjection;

import java.util.List;

@Mapper
public interface ApplicationDetailsRepresentationConverter {

    ApplicationDetailsRepresentation convertToRepresentation(ApplicationDetailsProjection applicationDetailsProjection);

    List<ApplicationDetailsRepresentation> convertToRepresentation(List<ApplicationDetailsProjection> applicationDetailsProjections);
}
