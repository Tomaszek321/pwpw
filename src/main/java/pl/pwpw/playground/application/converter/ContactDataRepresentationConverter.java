package pl.pwpw.playground.application.converter;

import org.mapstruct.Mapper;
import pl.pwpw.playground.application.ContactDataRepresentation;
import pl.pwpw.playground.domian.model.projection.ContactDetailsProjection;

@Mapper
public interface ContactDataRepresentationConverter {

    ContactDataRepresentation convertToRepresentation(ContactDetailsProjection contactDetailsProjection);
}
