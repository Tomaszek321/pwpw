package pl.pwpw.playground.application;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ExternalFile {

    private Long id;

    private byte [] content;

    private String type;
}
