package pl.pwpw.playground.application;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.pwpw.playground.application.converter.ApplicationDetailsRepresentationConverter;
import pl.pwpw.playground.application.converter.ContactDataRepresentationConverter;
import pl.pwpw.playground.domian.model.Application;
import pl.pwpw.playground.domian.model.Attachment;
import pl.pwpw.playground.domian.model.projection.ApplicationDetailsProjection;
import pl.pwpw.playground.domian.model.projection.ContactDetailsProjection;
import pl.pwpw.playground.domian.service.ApplicationService;
import pl.pwpw.playground.domian.service.AttachmentService;

import java.util.List;

@Service
@AllArgsConstructor
public class ApplicationApplicationService {

    private final AttachmentService attachmentService;
    private final ApplicationService applicationService;
    private final ContactDataRepresentationConverter contactDataConverter;
    private final ApplicationDetailsRepresentationConverter applicationDetailsConverter;

    @Transactional
    public void saveAttachment(final Long applicationId, final ExternalFile file) {
        final Attachment attachment = attachmentService.save(file);
        final Application application = applicationService.findById(applicationId);
        application.addAttachment(attachment);
    }

    @Transactional(readOnly = true)
    public ContactDataRepresentation getContactDataByApplicationId(final String appNr) {
        final ContactDetailsProjection projection = applicationService.findContactDetailsByApplicationNr(appNr);

       return contactDataConverter.convertToRepresentation(projection);
    }

    @Transactional(readOnly = true)
    public List<ApplicationDetailsRepresentation> getApplicationDetailsByEmail(final String email) {
        final List<ApplicationDetailsProjection> projections = applicationService.findApplicationDetailsByEmail(email);

        return applicationDetailsConverter.convertToRepresentation(projections);
    }

}
