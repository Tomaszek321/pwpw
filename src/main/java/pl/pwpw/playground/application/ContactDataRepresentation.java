package pl.pwpw.playground.application;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class ContactDataRepresentation {

    private String email;
    private String phone;
}
