package pl.pwpw.playground.application;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class ApplicationDetailsRepresentation {

    private String type;

    private String appNr;

    private String lastName;
}
