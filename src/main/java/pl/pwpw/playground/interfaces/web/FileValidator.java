package pl.pwpw.playground.interfaces.web;

import org.apache.commons.io.FilenameUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public class FileValidator {

    private static final String PDF = "pdf";
    private static final String JPG = "jpg";
    private static final String APPLICATION_PDF = "application/pdf";
    private static final String CONTENT_TYPE_JPG = "image/jpg";
    public static final String ERROR_MESSAGE = "Invalid file";

    public static byte[] validateMultipartFile(final MultipartFile multipartFile) throws IOException {
        validateFileType(multipartFile);
        validateContentType(multipartFile);
        validateFileExtension(multipartFile);

        return multipartFile.getBytes();
    }

    private static void validateFileExtension(MultipartFile multipartFile) throws IOException {
        final String fileExtension = FilenameUtils.getExtension(multipartFile.getOriginalFilename());
        if (!fileExtension.equals(JPG)
                && !fileExtension.equals(PDF)) {
            throw new IOException(ERROR_MESSAGE);
        }
    }

    private static void validateContentType(MultipartFile multipartFile) throws IOException {
        if (!multipartFile.getContentType().equals(CONTENT_TYPE_JPG)
                && !multipartFile.getContentType().equals(APPLICATION_PDF)) {
            throw new IOException(ERROR_MESSAGE);
        }
    }

    private static void validateFileType(MultipartFile multipartFile) throws IOException {
        final String attachmentType = getFileType(multipartFile.getBytes());

        if (!attachmentType.equals(PDF) && !attachmentType.equals(JPG)) {
            throw new IOException(ERROR_MESSAGE);
        }
    }

    private static String getFileType(final byte[] fileData) throws IOException {
        if (Byte.toUnsignedInt(fileData[0]) == 0x25 && Byte.toUnsignedInt(fileData[1]) == 0x50) {
            return PDF;
        } else {
            if (Byte.toUnsignedInt(fileData[0]) == 0xFF && Byte.toUnsignedInt(fileData[1]) == 0xD8)
                return JPG;
        }
        throw new IOException(ERROR_MESSAGE);
    }
}
