package pl.pwpw.playground.interfaces.web;

import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pl.pwpw.playground.application.ApplicationApplicationService;
import pl.pwpw.playground.application.ApplicationDetailsRepresentation;
import pl.pwpw.playground.application.ContactDataRepresentation;
import pl.pwpw.playground.application.ExternalFile;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("playgraound/application")
@AllArgsConstructor
public class ApplicationController {

    final ApplicationApplicationService applicationService;

    @PostMapping(value = "/{appId}/attachment", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity uploadFile(@PathVariable("appId") Long appId, @RequestParam("file") MultipartFile multipartFile) throws IOException {

        FileValidator.validateMultipartFile(multipartFile);

        final ExternalFile externalFile = new ExternalFile();
        final String type = multipartFile.getContentType();
        externalFile.setId(appId);
        externalFile.setType(type);
        externalFile.setContent(multipartFile.getBytes());

        applicationService.saveAttachment(appId, externalFile);

        return ResponseEntity.ok().build();
    }

    @PostMapping(value = "/contacts")
    public ContactDataRepresentation getContactDataForApplication(@RequestParam("appNr") final String appNr) {
        return applicationService.getContactDataByApplicationId(appNr);
    }

    @GetMapping(value = "/details/{email}")
    public List<ApplicationDetailsRepresentation> getApplicationDetails(@PathVariable("email") String email) {
        return applicationService.getApplicationDetailsByEmail(email);
    }
}

